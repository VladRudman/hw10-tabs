
// "use strick";

// Ждем пока загрузится DOM
document.addEventListener('DOMContentLoaded', () => {
  // Получаем все табы и тексты
  const tabs = document.querySelectorAll('.tabs-title');
  const texts = document.querySelectorAll('.textLi');

  // Функция для скрытия текста
  function hideTabsContent() {
    texts.forEach((text) => {
      text.classList.remove('active');
    });
  }

  // Функция для отображения текста
  function showTabContent(tab) {
    document.querySelector(`#${tab}`).classList.add('active');
  }

  // По умолчанию скрываем все тексты
  hideTabsContent();
  // По умолчанию отображаем текст первой вкладки
  showTabContent(tabs[0].getAttribute('data-tab'));
  // Делаем таким образом, что бы при клике на вкладку она становилась активной
  tabs.forEach((tab) => {
  tab.addEventListener('click', (event) => {
    // Убираем класс `active` у всех вкладок
    tabs.forEach((tab) => tab.classList.remove('active'));
    // Добавляем класс `active` текущей вкладке
    event.target.classList.add('active');

    // Продолжаем работу функции как обычно
    const tabName = event.target.getAttribute('data-tab');
    hideTabsContent();
    showTabContent(tabName);
  });
});

  // Добавляем обработчик события клик на каждую вкладку
  tabs.forEach((tab) => {
    tab.addEventListener('click', (event) => {
      // Получаем имя вкладки
      const tabName = event.target.getAttribute('data-tab');
      // Скрываем все тексты
      hideTabsContent();
      // Отображаем текст текущей вкладки
      showTabContent(tabName);
      
    });
  });
});